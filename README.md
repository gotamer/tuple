Tuple for Go
============

A tuple is an ordered list of mixed type elements

 * Key is of type uint
 * Value is of type interface and can hold therefore a mixed type of elements

*Note: index starts with 0*
```go
b := Tuple.New(1, 2, "three", 4, "five", 6)
bk := b.Add("appending") // bk == 6
i := b.Get(2)  // result is "three"
f := b.Find("five") // result 4
```

Links
-------
 * [Pkg Documentationn](http://go.pkgdoc.org/bitbucket.org/gotamer/tuple "Pkg Documentation")
 * [Repository](https://bitbucket.org/gotamer/tuple "Repository")
 * [Bug Tracking](https://bitbucket.org/gotamer/tuple/issues "Bug Tracking")
 * [Website for your comments](http://www.robotamer.com/html/GoTamer "GoTamer Website")
