// tuple_test.go
package tuple

import (
	//"fmt"
	"testing"
)

func TestNew(t *testing.T) {
	tuple := New("Zero", "one", "two", "three", "four", "five", "six")
	if v := tuple.Get(2); v != "two" {
		t.Errorf("New failed 2 should be two but is: %s", v)
	}
}

func TestAdd(t *testing.T) {
	tuple := New("Zero", "one", "two", "three", "four", "five", "six")
	tuple.Add("seven")
	if v := tuple.Get(7); v != "seven" {
		t.Errorf("Add failed 7 should be seven but is: %s", v)
	}
}

func TestFind(t *testing.T) {
	tuple := New("Zero", "one", "two", "three", "four", "five", "six")
	if k := tuple.Find("three"); k != 3 {
		t.Error("Find failed k should be key 3 but is: %s", k)
	}
}

func TestDel(t *testing.T) {
	tuple := New("Zero", "one", "two", "three", "four", "five", "six")
	tuple.Del(3)
	v := tuple.Get(3)
	if v != nil {
		t.Errorf("Del failed 3 should be <nil> but is: %s", v)
	}
	l := tuple.Len()
	if l != 6 {
		t.Error("Len failed should be 6")
	}
	tuple.Purge(3)
	v = tuple.Get(3)
	if v != "four" {
		t.Errorf("Purge failed 3 should be four but is: %s", v)
	}
	l = tuple.Len()
	if l != 6 {
		t.Error("Len failed should be 6")
	}
}

func TestSet(t *testing.T) {
	tuple := New("Zero", "one", "two", "three", "four", "five", "six")
	tuple.Set(3, "drei")
	tuple.Set(10, "TEN")
	if v := tuple.Get(3); v != "drei" {
		t.Errorf("Set failed 3 should be drei but is: %s", v)
	}
	if v := tuple.Get(10); v != "TEN" {
		t.Errorf("Set failed 10 should be TEN but is: %s", v)
	}
	if v := tuple.Get(9); v != nil {
		t.Errorf("Set failed 9 should be <nil> but is: %v", v)
	}
}

func TestMinMax(t *testing.T) {
	tuple := New("Zero", "one", "two", "three", "four", "five", "six")
	tuple.Del(0)
	if k := tuple.Min(); k != 1 {
		t.Errorf("Min failed min key should be 1 but is: %d", k)
	}
	if k := tuple.Max(); k != 6 {
		t.Errorf("Max failed max key should be 6 but is: %d", k)
	}
}

func TestWalker(t *testing.T) {
	tuple := New("Zero", "one", "two", "three", "four", "five", "six")
	var k uint
	for v := range tuple.Walker() {
		if k == 4 && v != "four" {
			t.Errorf("Walker failed 4 should be four but is: %s", v)
		}
		//fmt.Println(k, " => ", v)
		k++
	}
}
