// gotamer/tuple
// A tuple is an ordered list of mixed type elements
//
// Copyright © 2013 Dennis T Kaplan <http://www.robotamer.com>
//
// Use of this source code is governed by a MIT style license
// that can be found in the README file.
package tuple

import (
	"sync"
)

type Tuple struct {
	mutex sync.RWMutex
	T     map[uint]interface{}
}

// New will create and fill a Tuple with v mixed values
func New(i ...interface{}) (t *Tuple) {
	t = new(Tuple)
	t.T = make(map[uint]interface{})
	for c, r := range i {
		t.T[uint(c)] = r
	}
	return t
}

// Add will append a value to your Tuple and return its key
func (t *Tuple) Add(v interface{}) uint {
	t.mutex.Lock()
	defer t.mutex.Unlock()
	c := uint(len(t.T))
	t.T[c] = v
	return c
}

// Set will replace the value of the given key k with the given value v
func (t *Tuple) Set(k uint, v interface{}) {
	t.mutex.Lock()
	defer t.mutex.Unlock()
	t.T[k] = v
}

// Here for backwords compatibility
func (t *Tuple) Edit(k uint, v interface{}) {
	t.Set(k, v)
}

// Del will remove the entry with the given index key k, and reindex the keys
func (t *Tuple) Del(k uint) {
	t.mutex.Lock()
	defer t.mutex.Unlock()
	delete(t.T, k)
}

// Purge will remove the entry with the given index key k, and reindex the keys
func (t *Tuple) Purge(k uint) {
	t.mutex.Lock()
	defer t.mutex.Unlock()
	var i uint
	j := uint(len(t.T)) - 1
	for ; i < j; i++ {
		t.T[i] = t.T[i+1]
	}
	delete(t.T, i)
}

// ReKey will reindex the keys
func (t *Tuple) ReKey() {
	tn := make(map[uint]interface{})
	t.mutex.Lock()
	defer t.mutex.Unlock()
	ks := t.keys()
	for kn, ko := range ks {
		tn[uint(kn)] = t.T[ko]
	}
	t.T = tn
}

// Get will return the value of the given index key k
func (t *Tuple) Get(k uint) interface{} {
	t.mutex.RLock()
	defer t.mutex.RUnlock()
	return t.T[k]
}

// Has will check if an elemet exists
func (t *Tuple) Has(i interface{}) bool {
	t.mutex.RLock()
	defer t.mutex.RUnlock()
	for v := range t.Walker() {
		if v == i {
			return true
		}
	}
	return false
}

// Find will return the index key for the given value i
func (t *Tuple) Find(i interface{}) (k uint) {
	t.mutex.RLock()
	defer t.mutex.RUnlock()
	for k, v := range t.T {
		if v == i {
			return k
		}
	}
	return
}

// Len will return the count of items in the tuple
// A Tuple with index keys from 0 to 5 returns length of 6
func (t *Tuple) Len() uint {
	t.mutex.RLock()
	defer t.mutex.RUnlock()
	return uint(len(t.T))
}

// Keys returns a slice list of keys from the given Tuple
func (t *Tuple) Keys() []uint {
	t.mutex.RLock()
	defer t.mutex.RUnlock()
	return t.keys()
}

// Internal, without mutex
func (t *Tuple) keys() (ks []uint) {
	for k, _ := range t.T {
		ks = append(ks, k)
	}
	return
}

// Min returns the smallest key from the given Tuple
func (t *Tuple) Min() (m uint) {
	t.mutex.RLock()
	defer t.mutex.RUnlock()
	l := t.keys()
	m = l[0]
	for _, v := range l {
		if v < m {
			m = v
		}
	}
	return
}

// Max returns the largest key from the given Tuple
func (t *Tuple) Max() (m uint) {
	t.mutex.RLock()
	defer t.mutex.RUnlock()
	l := t.keys()
	m = l[0]
	for _, v := range l {
		if v > m {
			m = v
		}
	}
	return
}

// The Walker() iterates over the Tuple map and returns them in FIFO order.
//		for i := range Walker() {
//			fmt.Println(i)
//		}
func (t *Tuple) Walker2() (c chan interface{}) {
	c = make(chan interface{})
	t.mutex.RLock()
	defer t.mutex.RUnlock()
	go func() {
		l := uint(len(t.T))
		for i := uint(0); i < l; i++ {
			c <- t.T[i]
		}
		close(c)
	}()
	return
}

// The Walker() iterates over the Tuple map and returns them in FIFO order.
//		for i := range Walker() {
//			fmt.Println(i)
//		}
func (t *Tuple) Walker() (c chan interface{}) {
	c = make(chan interface{})
	t.mutex.RLock()
	defer t.mutex.RUnlock()
	ks := t.keys()
	go func() {
		for _, k := range ks {
			c <- t.T[k]
		}
		close(c)
	}()
	return
}
